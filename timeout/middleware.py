# This file is part of django-timeout.
# 
# django-timeout is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# django-timeout is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with django-timeout.  If not, see <http://www.gnu.org/licenses/>.

from django.conf import settings
from django.contrib.auth import logout

import time

# TIMEOUT_TIMEOUT_TIME is the maximum time alloted before timeout in seconds
try:  # try loading TIMEOUT_TIMEOUT_TIME from settings
    TIMEOUT_TIMEOUT_TIME = settings.TIMEOUT_TIMEOUT_TIME
except AttributeError:
    TIMEOUT_TIMEOUT_TIME = 30 * 60  # default to 30 minutes

# TIMEOUT_KEY_TIMEOUT is the session key used to signify a timeout error
try:  # try loading TIMEOUT_KEY_TIMEOUT from settings
    TIMEOUT_KEY_TIMEOUT = settings.TIMEOUT_KEY_TIMEOUT
except AttributeError:
    TIMEOUT_KEY_TIMEOUT = 'timeout_timeout'

# TIMEOUT_KEY_TIME is the session key used to hold the time
try:  # try loading TIMEOUT_KEY_TIME from settings
    TIMEOUT_KEY_TIME = settings.TIMEOUT_KEY_TIME
except AttributeError:
    TIMEOUT_KEY_TIME = 'timeout_last_activity'


class TimeoutMiddleware():
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        # before view
        if request.user.is_authenticated:
            # remove TIMEOUT_KEY_TIMEOUT from session
            if TIMEOUT_KEY_TIMEOUT in request.session.keys():
                del request.session[TIMEOUT_KEY_TIMEOUT]
            # if a previous time is stored
            if TIMEOUT_KEY_TIME in request.session.keys():
                # get the change in time from previous request to this request
                time_delta = time.time() - request.session.get(TIMEOUT_KEY_TIME)
                # check if the change in time is greater than allowed
                if time_delta > TIMEOUT_TIMEOUT_TIME:
                    # if so, logout and signify with session
                    logout(request)
                    request.session[TIMEOUT_KEY_TIMEOUT] = "true"
            # store the current time in the session
            request.session[TIMEOUT_KEY_TIME] = time.time()
        # if the user is not logged in and TIMEOUT_KEY_TIME is in session, remove it
        elif TIMEOUT_KEY_TIME in request.session.keys():
            del request.session[TIMEOUT_KEY_TIME]

        response = self.get_response(request)

        # after view

        return response
