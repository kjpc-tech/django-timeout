==============
django-timeout
==============

Session timeout for Django.

Updated for django 1.10

Quick start
-----------

1. Add "timeout" to your INSTALLED_APPS.
2. Add 'timeout.middleware.TimeoutMiddleware' to MIDDLEWARE.
3. Adjust timeout settings (in your settings.py) as necessary:

	- TIMEOUT_TIMEOUT_TIME is the timeout time in seconds, defaults to 30 * 60 (30 minutes)
	- TIMEOUT_KEY_TIMEOUT is the session key that indicates if a user session has timed out, defaults to 'timeout_timeout'

		- request.session[TIMEOUT_KEY_TIMEOUT] will hold "true" if a timeout occurred

	- TIMEOUT_KEY_TIME is the session key that holds the last activity time, defaults to 'timeout_last_activity'

Source
------

This was originally found on Stack Overflow here http://stackoverflow.com/questions/9267957/is-there-a-way-to-combine-behavior-of-session-expire-at-browser-close-and-sessio/9290334#9290334. I have modified it since but original credit goes to danihp.

License
-------

Since the original code is under the GNU license, so is this.