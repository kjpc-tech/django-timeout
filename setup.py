from setuptools import find_packages, setup

setup(
    name='django-timeout',
    version='0.2',
    packages=find_packages(),
    author='Kyle (Originally danihp on SO)',
    author_email='kyle@kjpc.tech',
    license='GNU GPLv3 License',
    description='Django middleware that monitors session activity and logs out user if they are inactive. Originally found here http://stackoverflow.com/a/9290334/5286674.',
    url='https://bitbucket.org/kjpc-tech/django-timeout',
    include_package_data=True
)
